export class EventHistory {
    id: string;
    title: string;
    summary: string;
    imageSrc: string;
}
