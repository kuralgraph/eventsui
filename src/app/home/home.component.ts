import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';
import { EventHistory } from '../domain/event-history';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  name = 'Angular';
  //ids: Array<String> = ['one', 'two', 'three', 'four', 'onex', 'twox', 'threex', 'fourx']
  
  ids: Array<any> = [];
  idsx: Array<any> = [];

  constructor( private data: DataService ) { }

  ee: EventHistory[];

  ngOnInit() {
    this.data.getEvents()
      .subscribe( e => {
        this.ids = e;

       for (let prop in e) {
         this.idsx.push(prop);
      }
      });
  };
}

