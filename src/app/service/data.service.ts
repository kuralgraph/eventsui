import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { EventHistory } from '../domain/event-history';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) {}
  private userUrl = 'http://localhost:9090/api/timeline/v1/events';

  public getEvents() {
    return this.http.get<EventHistory[]>(this.userUrl);
  }

}
